let students = ['John', 'Joe', 'Jane', 'Jessie']
let students1 = [ 1, 'a', 'b']
let students2 = ['e', 'ae', 'sdsdse']
let empty1 = []

/**
 * No 1
 * 
 * @param {Array} students
 * @param {String} string
 */
function addToEnd(students, string) {
    if( typeof string != 'string'){
        return "error - can only add strings to an array"
    }
    
    students.push(string)
    return students
}
// console.log(addToEnd(students, 1))

/**
 * No 2
 * 
 * @param {Array} students
 * @param {String} string
 */
 function addToStart(students, string) {
    if( typeof string != 'string'){
        return "error - can only add strings to an array"
    }
    
    students.unshift(string)
    return students
}
// console.log(addToStart(students, "1"))

/**
 * No 3
 * 
 * @param {Array} students
 * @param {String} string
 */
 function elementChecker(students, string) {
    if( students.length === 0) {
        return "error - passed in array is empty"
    }
    
    return students.some((el) => el === string)
}
// console.log(elementChecker(students, "saSA"))

/**
 * No 4
 * 
 * @param {Array} students
 * @param {String} string
 */
 function checkAllStringsEnding(students, string) {
    if( students.length === 0) {
        return "error - must NOT be empty"
    }
    
    if(students.some((el) => typeof el !== 'string')) {
        return "error - all array elements must be strings"
    }

    if(typeof string !== "string") {
        return "error - 2nd argument must be of data type string"
    }

    if(string.length > 1) {
        return "error - 2nd argument must be a single character"
    }

    return students.every((el) => el[el.length-1] == string)
}
// console.log(checkAllStringsEnding(empty1, "saSA"))
// console.log(checkAllStringsEnding(students1, "sass"))
// console.log(checkAllStringsEnding(students, 1))
// console.log(checkAllStringsEnding(students,'ee'))
// console.log(checkAllStringsEnding(students,'e'))
// console.log(checkAllStringsEnding(students2,'e'))


/**
 * No 5
 * 
 * @param {Array} students
 */
 function stringLengthSorter(students) {

    
    if(students.some((el) => typeof el !== 'string')) {
        return "error - all array elements must be strings"
    }

    return students.sort()
}
// console.log(stringLengthSorter(students))
// console.log(stringLengthSorter(students1))


/**
 * No 6
 * 
 * @param {Array} students
 * @param {String} string
 */
 function startsWithCounter(students, string) {
    if( students.length === 0) {
        return "error - must NOT be empty"
    }
    
    if(students.some((el) => typeof el !== 'string')) {
        return "error - all array elements must be strings"
    }

    if(typeof string !== "string") {
        return "error - 2nd argument must be of data type string"
    }

    if(string.length > 1) {
        return "error - 2nd argument must be a single character"
    }

    return students.filter((el) => el.startsWith(string) || el.startsWith(string.toUpperCase())).length
}

// console.log(startsWithCounter(students, 'j'))


/**
 * No 7
 * 
 * @param {Array} students
 * @param {String} string
 */
 function likeFinder(students, string) {
    if( students.length === 0) {
        return "error - must NOT be empty"
    }
    
    if(students.some((el) => typeof el !== 'string')) {
        return "error - all array elements must be strings"
    }

    if(typeof string !== "string") {
        return "error - 2nd argument must be of data type string"
    }

    const newArray = students.map(element => {
        if (element.toLowerCase().includes(string.toLowerCase())) {
          return element;
        }
    })

    return newArray.filter(element => {
        return element !== undefined;
    });
}
// console.log(likeFinder(students, "Jo"))

/**
 * No 8
 * 
 * @param {Array} students
 */
 function randomPicker(students) {
    const randomElement = students[Math.floor(Math.random() * students.length)]
    return randomElement

}
// console.log(randomPicker(students))