let studentOneName = "John"
let studentOneEmail = "john@mail.com"
let studentOneGrades = [89, 84, 78, 88]

let studentTwoName = "Joe"
let studentTwoEmail = "joe@mail.com"
let studentTwoGrades = [78, 82, 79, 85]

let studentThreeName = "Jane"
let studentThreeEmail = "jane@mail.com"
let studentThreeGrades = [87, 89, 91, 93]

let studentFourName = "Jessie"
let studentFourEmail = "jessie@mail.com"
let studentFourGrades = [91, 89, 92, 93]


//Translate the other students from our boilerplate code into their own respective objects.
let studentOne = {
    name: "John", 
    email: "john@mail.com",
    grades: [89, 84, 78, 88],
    computeAve(){
        return this.grades.reduce((a, b) => a + b, 0)/4
    },
    willPass() {
        if(this.computeAve() >= 85) return true
        return false
    },
    willPassWithHonors() {
        if(this.computeAve() >= 90) {
            return true
        } else if (this.computeAve() < 90 && this.computeAve() >= 85) {
            return false
        }
    }
}

let studentTwo = {
    name: "Joe", 
    email: "joe@mail.com",
    grades: [78, 82, 79, 85],
    computeAve(){
        return this.grades.reduce((a, b) => a + b, 0)/4
    },
    willPass() {
        if(this.computeAve() >= 85) return true
        return false
    },
    willPassWithHonors() {
        if(this.computeAve() >= 90) {
            return true
        } else if (this.computeAve() < 90 && this.computeAve() >= 85) {
            return false
        }
    }
}

let studentThree = {
    name: "Jane", 
    email: "jane@mail.com",
    grades: [87, 89, 91, 93],
    computeAve(){
        return this.grades.reduce((a, b) => a + b, 0)/4
    },
    willPass() {
        if(this.computeAve() >= 85) return true
        return false
    },
    willPassWithHonors() {
        if(this.computeAve() >= 90) {
            return true
        } else if (this.computeAve() < 90 && this.computeAve() >= 85) {
            return false
        }
    }
}

let studentFour = {
    name: "Jessie", 
    email: "jessie@mail.com",
    grades: [91, 89, 92, 93],
    computeAve() {
        return this.grades.reduce((a, b) => a + b, 0)/4
    },
    willPass() {
        if(this.computeAve() >= 85) return true
        return false
    },
    willPassWithHonors() {
        if(this.computeAve() >= 90) {
            return true
        } else if (this.computeAve() < 90 && this.computeAve() >= 85) {
            return false
        }
    }
}


//Define a method for EACH student object that will compute for their grade average (total of grades divided by 4)
    // console.log(studentOne.computeAve())
    // console.log(studentTwo.computeAve())
    // console.log(studentThree.computeAve())
    // console.log(studentFour.computeAve())


//Define a method for all student objects named willPass() that returns a Boolean value indicating if student will pass or fail. For a student to pass, their ave. grade must be greater than or equal to 85.
    // console.log(studentOne.willPass())
    // console.log(studentTwo.willPass())
    // console.log(studentThree.willPass())
    // console.log(studentFour.willPass())


//Define a method for all student objects named willPassWithHonors() that returns true if ave. grade is greater than or equal to 90, false if >= 85 but < 90, and undefined if < 85 (since student will not pass).
    // console.log(studentOne.willPassWithHonors())
    // console.log(studentTwo.willPassWithHonors())
    // console.log(studentThree.willPassWithHonors())
    // console.log(studentFour.willPassWithHonors())

//Create an object named classOf1A with a property named students which is an array containing all 4 student objects in it.
let classOf1A = {
    students: [
        studentOne,
        studentTwo,
        studentThree,
        studentFour,
    ],
    countHonorStudents(){
        return this.students.filter((student)=> {
            return student.willPassWithHonors()
        }).length
    },
    honorsPercentage(){
        return `${this.countHonorStudents()/this.students.length * 100}%`
    },
    retrieveHonorStudentInfo(){
        let filtered = this.students.filter((student)=>{
            return student.willPassWithHonors()
        })

        return filtered.map((student)=>{
                return {
                   "aveGrade": student.computeAve(),
                    "email": student.email,
                }
        })
    },
    sortHonorStudentsByGradeDesc(){
        return this.retrieveHonorStudentInfo().sort((a,b)=> b.aveGrade - a.aveGrade)
    }
}

//Create a method for the object classOf1A named countHonorStudents() that will return the number of honor students.
    // console.log(classOf1A.countHonorStudents())

//Create a method for the object classOf1A named honorsPercentage() that will return the % of honor students from the batch's total number of students.
    // console.log(classOf1A.honorsPercentage())

//Create a method for the object classOf1A named retrieveHonorStudentInfo() that will return all honor students' emails and ave. grades as an array of objects.
    // console.log(classOf1A.retrieveHonorStudentInfo())

//Create a method for the object classOf1A named sortHonorStudentsByGradeDesc() that will return all honor students' emails and ave. grades as an array of objects sorted in descending order based on their grade averages.
    // console.log(classOf1A.sortHonorStudentsByGradeDesc())
