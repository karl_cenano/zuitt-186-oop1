class Student {
    //a constructor method defines How an object's structure is instantiated from the class
    constructor(name, email, grades, passed, passedWithHonors){
        this.name = name
        this.email = email
        this.gradeAve = undefined
        if(passed){
            this.passed = passed
        }else{
            this.passed=undefined
        }

        if(passedWithHonors){
            this.passedWithHonors = passedWithHonors
        }else{
            this.passedWithHonors = undefined
        }

        if(grades.length === 4) {
            if(grades.every(grade => typeof grade === "number")){
                if(grades.every(grade => grade >= 0 && grade <= 100)){
                    this.grades = grades
                }else{
                    this.grades = undefined
                }
            }else{
                this.grades = undefined
            }
        }else{
            this.grades = undefined
        }
    }
    //methods
    //this -> object
    login(){
        console.log(`${this.email} has logged in`)
        return this //method chaining //ex. studentOne.login().logout()
    }
    logout(){
        console.log(`${this.email} has logged out`)
        return this
    }
    listGrades(){
        console.log(`${this.name}'s quarterly averages are: ${this.grades}`)
        return this
    }
    computeAve(){
        let sum = 0
        this.grades.forEach(grade => sum = sum + grade)
        this.gradeAve = sum/4
        return this
    }

    willPass(){
        this.computeAve()
		this.passed = this.gradeAve >= 85 ? true : false
        return this
	}

	willPassWithHonors(){
        this.computeAve()
		if(this.gradeAve >= 90){
            this.passedWithHonors = true
			return this
		}else if(this.gradeAve < 90 && this.gradeAve >= 85){
			this.passedWithHonors = false
            return this
		}else {
			this.passedWithHonors = undefined
            return this
		}
	}

}

let studentOne = new Student("John", "john@mail.com", [89,84,78,88])
let studentTwo = new Student("Joe", "joe@mail.com", [78, 82, 79, 85])
let studentThree = new Student("Jane", "jane@mail.com", [87, 89, 91, 93])
let studentFour = new Student("Jessie", "jessie@mail.com", [91, 89, 92, 93])

//Modify the Student class to have a new constructor property named "passed" and another new constructor property named "passedWithHonors". Both properties should be given a value of "undefined" by default.
    // console.log(studentOne)

//Modify our willPass() and willPassWithHonors() methods from the previous session to be both chainable, and assign the values they return to the new "passed" and "willPassWithHonors" properties in our constructor
    // console.log(studentFour.willPass().willPassWithHonors())