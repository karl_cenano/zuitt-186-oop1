let studentOneName = "John"
let studentOneEmail = "john@mail.com"
let studentOneGrades = [89, 84, 78, 88]

let studentTwoName = "Joe"
let studentTwoEmail = "joe@mail.com"
let studentTwoGrades = [78, 82, 79, 85]

let studentThreeName = "Jane"
let studentThreeEmail = "jane@mail.com"
let studentThreeGrades = [87, 89, 91, 93]

let studentThreeName = "Jessie"
let studentThreeEmail = "jessie@mail.com"
let studentThreeGrades = [91, 89, 92, 93]

class Student {
    //a constructor method defines How an object's structure is instantiated from the class
    constructor(name, email, grades){
        this.name = name
        this.email = email
        this.gradeAve = undefined

        if(grades.length === 4) {
            if(grades.every(grade => typeof grade === "number")){
                if(grades.every(grade => grade >= 0 && grade <= 100)){
                    this.grades = grades
                }else{
                    this.grades = undefined
                }
            }else{
                this.grades = undefined
            }
        }else{
            this.grades = undefined
        }
    }
    //methods
    //this -> object
    login(){
        console.log(`${this.email} has logged in`)
        return this //method chaining //ex. studentOne.login().logout()
    }
    logout(){
        console.log(`${this.email} has logged out`)
        return this
    }
    listGrades(){
        console.log(`${this.name}'s quarterly averages are: ${this.grades}`)
        return this
    }
    computeAve(){
        let sum = 0
        this.grades.forEach(grade => sum = sum + grade)
        this.gradeAve = sum/4
        return this
    }
}

let studentOne = new Student("John", "john@mail.com", [89,84,78,88])
let studentTwo = new Student("Joe", "joe@mail.com", [78, 82, 79, 85])
let studentTwo = new Student("Jane", "jane@mail.com", [87, 89, 91, 93])
let studentTwo = new Student("Jessie", "jessie@mail.com", [91, 89, 92, 93])



console.log(studentOne)