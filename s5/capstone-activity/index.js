class Customer {
    constructor(email) {
        this.email = email,
        this.cart = new Cart(),
        this.orders = []
    }

    checkout(){
        this.cart.computeTotal()
        this.orders.push({
            products: this.cart.contents,
            totalAmount: this.cart.totalAmount
        })

        return this
    }
}

class Cart {
    constructor(){
        this.contents = [],
        this.totalAmount = 0
    }

    addToCart(product, quantity) {
        this.contents.push({
            product,
            quantity
        })

        return this
    }

    showCartContents() {
        console.log(this.contents)

        return this
    }

    updateProductQuantity(product, updateQuantity) {
        let productFound = this.contents.find((el) => {
            return el.product.name === product
        })

        if(productFound) {
            productFound.quantity = updateQuantity
        }

        return this
    }

    clearCartContents() {
        this.contents = []

        return this
    }

    computeTotal() {
        this.contents.map(({product, quantity})=>{
            this.totalAmount += product.price * quantity
        })

        return this
    }


}

class Product {
    constructor(name, price){
        this.name = name,
        this.price = price,
        this.isActive = true
    }

    archive(){
        this.isActive = false

        return this
    }

    updatePrice(price){
        this.price = price

        return this
    }
}


const john = new Customer('john@mail.com')
const prodA = new Product('soap', 9.99)
const prodB = new Product('fish', 10.99)

// prodA.updatePrice(12.99).archive()
// console.log('john:', john)
// console.log('prodA:', prodA)

// console.log(
    john.cart
        .addToCart(prodA, 3)
        // .addToCart(prodB, 1)
        // .showCartContents()
        // .updateProductQuantity('soap', 5)
        // .clearCartContents()
        // .computeTotal()
// )

console.log(john.checkout())